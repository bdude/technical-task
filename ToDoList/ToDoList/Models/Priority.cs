﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ToDoList.Models
{
    public class Priority
    {
        public Priority()
        {
            TasksToDo = new HashSet<TaskToDo>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(32)]
        [Required]
        public string Name { get; set; }

        public ICollection<TaskToDo> TasksToDo { get; set; }
    }
}
