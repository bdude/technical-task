﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ToDoList.Models
{
    public partial class ToDoListDbContext : DbContext
    {
        public ToDoListDbContext()
            : base()
        {
        }
        public ToDoListDbContext(DbContextOptions<ToDoListDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<UserTaskList>()
                .HasIndex(u => u.UserName)
                .IsUnique();
        }

        public DbSet<Priority> Priority { get; set; }
        public DbSet<TaskToDo> TaskToDo { get; set; }
        public DbSet<UserTaskList> TaskList { get; set; }
    }
}
