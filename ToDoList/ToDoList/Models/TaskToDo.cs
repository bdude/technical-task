﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ToDoList.Models
{
    public class TaskToDo
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [StringLength(128)]
        [Required]
        public string Name { get; set; }
        [Required]
        public UserTaskList TaskList { get; set; }
        [StringLength(1024)]
        public string Description { get; set; }
        [DisplayName("Due date")]
        public DateTime? DueDate { get; set; }
        public int? PriorityId { get; set; }
        public int? TaskListId { get; set; }
        public bool Done { get; set; }   
        public Priority Priority { get; set; }   
    }
}
