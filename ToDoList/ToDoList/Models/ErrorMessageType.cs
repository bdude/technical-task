﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoList.Models
{
    public class ErrorMessageType
    {
        private ErrorMessageType(string value) { Message = value; }

        public string Message { get; set; }

        public static ErrorMessageType AccessDenied { get { return new ErrorMessageType("You don't have permission to access this resource"); } }
        public static ErrorMessageType NotFound { get { return new ErrorMessageType("Entry not found in the database"); } }
    }
}
