using System;

namespace ToDoList.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }
        public ErrorMessageType ErrorMessage { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}