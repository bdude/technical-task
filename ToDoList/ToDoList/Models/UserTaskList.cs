﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoList.Models
{
    public class UserTaskList
    {
        public UserTaskList(string userName)
        {
            UserName = userName;
            TasksToDo = new HashSet<TaskToDo>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string UserName { get; set; }
        public ICollection<TaskToDo> TasksToDo { get; set; }
    }
}
