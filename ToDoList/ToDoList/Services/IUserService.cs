﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoList.Services
{
    public interface IUserService
    {
        bool IsAuthenticated();
        List<string> GetUserGroups();
        string GetUserIdentificator();
        string GetUserName();
    }
}
