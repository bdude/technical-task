﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoList.Tenant
{
    public class TenantProvider : ITenantProvider
    {
        private List<Tenant> tenants;
        public List<Tenant> GetAllTenants()
        {
            LoadTenants();
            return tenants;
        }

        private void LoadTenants()
        {
            if (this.tenants != null && this.tenants.Count > 0)
                return;

            var tenants = File.ReadAllText("tenants.json");
            this.tenants = JsonConvert.DeserializeObject<List<Tenant>>(tenants);
        }
    }
}
