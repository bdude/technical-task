﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoList.Tenant
{
    public interface ITenantProvider
    {
        List<Tenant> GetAllTenants();
    }
}
