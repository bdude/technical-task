﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoList.Tenant
{
    public sealed class Tenant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool HasAdminRights { get; set; }
    }
}
