﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using ToDoList.Services;

namespace ToDoList.Tenant
{
    public class AzureADTenantService : ITenantService
    {
        private readonly ITenantProvider _tenantProvider;
        private readonly IConfiguration _configuration;
        private readonly IUserService _userService;

        private readonly string AdminTenantName = "admin";
        private readonly string UserTenantName = "user";
        public AzureADTenantService(IConfiguration configuration, IUserService userSevice, ITenantProvider tenantProvider)
        {
            _configuration = configuration;
            _tenantProvider = tenantProvider;
            _userService = userSevice;
        }

        public List<Tenant> GetAllTenants()
        {
            return _tenantProvider.GetAllTenants();
        }

        public Tenant GetCurrentTenant()
        {
            if (_userService.IsAuthenticated())
            {
                List<string> groups = _userService.GetUserGroups();

                if (groups.Contains(_configuration["AdminGroupId"]))
                    return _tenantProvider.GetAllTenants().FirstOrDefault(x => x.Name == AdminTenantName);
                else
                    return _tenantProvider.GetAllTenants().FirstOrDefault(x => x.Name == UserTenantName);
            }

            else
                return _tenantProvider.GetAllTenants().FirstOrDefault(x => x.Name == UserTenantName);
        }
    }
}
