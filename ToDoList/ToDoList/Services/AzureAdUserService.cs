﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ToDoList.Services
{
    public class AzureAdUserService : IUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AzureAdUserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public List<string> GetUserGroups()
        {
            var identity = GetIdentity();
            return identity.Claims.Where(c => c.Type == "groups").Select(x => x.Value).ToList();
        }

        public string GetUserIdentificator()
        {
            var identity = GetIdentity();
            return identity.Claims.FirstOrDefault(c => c.Type.Contains("objectidentifier"))?.Value;

        }

        public string GetUserName()
        {
            var identity = GetIdentity();
            return identity.Claims.FirstOrDefault(c => c.Type == "preferred_username")?.Value;
        }

        public bool IsAuthenticated()
        {
            return _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated;
        }

        private ClaimsIdentity GetIdentity()
        {
            return _httpContextAccessor.HttpContext.User.Identity as ClaimsIdentity;
        }
    }
}
