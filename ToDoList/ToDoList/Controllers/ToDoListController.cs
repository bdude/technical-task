﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using ToDoList.Models;
using ToDoList.Services;
using ToDoList.Tenant;

namespace ToDoList.Controllers
{
    [Authorize]
    public class ToDoListController : Controller
    {
        private readonly ToDoListDbContext _context;
        private readonly ITenantService _tenantService;
        private readonly IUserService _userService;

        public ToDoListController(ToDoListDbContext context, ITenantService tenantService, IUserService userService)
        {
            _context = context;
            _tenantService = tenantService;
            _userService = userService;
        }

        public IActionResult Index()
        {
            IQueryable<UserTaskList> taskLists = _context.TaskList
                .Include(x => x.TasksToDo)
                .ThenInclude(x => x.Priority);

            // Non-admin can see only own lists
            if (!_tenantService.GetCurrentTenant().HasAdminRights)
                taskLists = taskLists.Where(x => x.UserName == _userService.GetUserName());
            else
                ViewData["IsAdmin"] = true;

            return View(taskLists.ToList());
            }

        public IActionResult Details(int id)
        {
            var taskToDo = GetTask(id);

            if (taskToDo == null)
                return View("Error", new ErrorViewModel { RequestId = GetRequestId(), ErrorMessage = ErrorMessageType.NotFound });

            if(!CanAccessTask(taskToDo))
                return View("Error", new ErrorViewModel { RequestId = GetRequestId(), ErrorMessage = ErrorMessageType.AccessDenied });

            return View(taskToDo);
        }

        public IActionResult Create()
        {
            ViewData["Priorities"] = new SelectList(_context.Priority, "Id", "Name");
            ViewData["IsAdmin"] = _tenantService.GetCurrentTenant().HasAdminRights;
            ViewData["Users"] = new SelectList(_context.TaskList, "UserName", "UserName");

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id,Name,Description,DueDate,PriorityId,Done")] TaskToDo taskToDo, string userName)
        {
            // If no username was sent - user is non-admin -> create record for his own
            if (userName == null)
                userName = _userService.GetUserName();

            UserTaskList taskList = _context.TaskList.FirstOrDefault(x => x.UserName == userName);
            if (taskList == null)
                return View("Error", new ErrorViewModel { RequestId = GetRequestId(), ErrorMessage = ErrorMessageType.NotFound });
            taskList.TasksToDo.Add(taskToDo);

            // Update model properties
            taskToDo.TaskList = taskList;
            taskToDo.Priority = _context.Priority.FirstOrDefault(x => x.Id == taskToDo.PriorityId);

            RevalidateModel(taskToDo);
            if (ModelState.IsValid)
            {
                _context.Add(taskToDo);
                _context.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
            
            ViewData["Priorities"] = new SelectList(_context.Priority, "Id", "Name", taskToDo.Priority.Name);
            ViewData["IsAdmin"] = _tenantService.GetCurrentTenant().HasAdminRights;
            ViewData["Users"] = new SelectList(_context.TaskList, "Id", "UserName", userName);

            return View(taskToDo);
        }


        public IActionResult Edit(int id)
        {
            var taskToDo = GetTask(id);

            if (taskToDo == null)
                return View("Error", new ErrorViewModel { RequestId = GetRequestId(), ErrorMessage = ErrorMessageType.NotFound });

            if (!CanAccessTask(taskToDo))
                return View("Error", new ErrorViewModel { RequestId = GetRequestId(), ErrorMessage = ErrorMessageType.AccessDenied });

            ViewData["Priorities"] = new SelectList(_context.Priority, "Id", "Name", taskToDo.Priority.Name);

            return View(taskToDo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,TaskListId,Name,Description,DueDate,PriorityId,Done")] TaskToDo taskToDo)
        {
            if (id != taskToDo.Id)
                return View("Error", new ErrorViewModel { RequestId = GetRequestId(), ErrorMessage = ErrorMessageType.NotFound });

            // Update model properties
            taskToDo.Priority = _context.Priority.FirstOrDefault(x => x.Id == taskToDo.PriorityId);
            taskToDo.TaskList = _context.TaskList.FirstOrDefault(x => x.Id == taskToDo.TaskListId);

            if (!CanAccessTask(taskToDo))
                return View("Error", new ErrorViewModel { RequestId = GetRequestId(), ErrorMessage = ErrorMessageType.AccessDenied });

            RevalidateModel(taskToDo);
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(taskToDo);
                    _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TaskToDoExists(taskToDo.Id))
                    {
                        return View("Error", new ErrorViewModel { RequestId = GetRequestId(), ErrorMessage = ErrorMessageType.NotFound });
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            ViewData["Priorities"] = new SelectList(_context.Priority, "Id", "Name", taskToDo.Priority.Name);

            return View(taskToDo);
        }


        public IActionResult Delete(int id)
        {
            var taskToDo = GetTask(id);

            if (taskToDo == null)
                return View("Error", new ErrorViewModel { RequestId = GetRequestId(), ErrorMessage = ErrorMessageType.NotFound });

            if (!CanAccessTask(taskToDo))
                return View("Error", new ErrorViewModel { RequestId = GetRequestId(), ErrorMessage = ErrorMessageType.AccessDenied });

            return View(taskToDo);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var taskToDo = GetTask(id);

            if (!CanAccessTask(taskToDo))
                return View("Error", new ErrorViewModel { RequestId = GetRequestId(), ErrorMessage = ErrorMessageType.AccessDenied });

            _context.TaskToDo.Remove(taskToDo);
            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }

        private bool TaskToDoExists(int id)
        {
            return _context.TaskToDo.Any(e => e.Id == id);
        }

        private void RevalidateModel(TaskToDo taskToDo)
        {
            ModelState.Clear();
            TryValidateModel(taskToDo);
        }

        private TaskToDo GetTask(int id)
        {
            return _context.TaskToDo
                .Include(x => x.Priority)
                .Include(x => x.TaskList)
                .FirstOrDefault(m => m.Id == id);
        }

        private string GetRequestId()
        {
            return Activity.Current?.Id ?? HttpContext.TraceIdentifier;
        }

        private bool CanAccessTask(TaskToDo task)
        {
            // Admin can access all tasks
            if (_tenantService.GetCurrentTenant().HasAdminRights)
                return true;

            // Other users can access only their own tasks
            return task.TaskList.UserName == _userService.GetUserName();
        }
    }
}
