﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebHooks;
using Microsoft.Extensions.Configuration;

namespace ToDoList.Controllers
{
    public class AzureAlertController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public AzureAlertController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [AzureAlertWebHook]
        public IActionResult AzureAlert(string id, string @event, AzureAlertNotification data)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            string log = string.Format("Status: {0}\r\nNotification name: {1}\r\nMetric name: {2}\r\nMetric value: {3}\r\n",
                data.Status, data.Context.Name, data.Context.Condition.MetricName,data.Context.Condition.MetricValue);

            System.IO.File.AppendAllText(_configuration["LogFile"], log);

            return Ok();
        }
    }
}
