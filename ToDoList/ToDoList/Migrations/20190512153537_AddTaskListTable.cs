﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ToDoList.Migrations
{
    public partial class AddTaskListTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserIdentificator",
                table: "TaskToDo");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "TaskToDo",
                maxLength: 128,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "TaskToDo",
                maxLength: 1024,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TaskListId",
                table: "TaskToDo",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Priority",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "TaskList",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskList", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TaskToDo_TaskListId",
                table: "TaskToDo",
                column: "TaskListId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskList_UserName",
                table: "TaskList",
                column: "UserName",
                unique: true,
                filter: "[UserName] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_TaskToDo_TaskList_TaskListId",
                table: "TaskToDo",
                column: "TaskListId",
                principalTable: "TaskList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskToDo_TaskList_TaskListId",
                table: "TaskToDo");

            migrationBuilder.DropTable(
                name: "TaskList");

            migrationBuilder.DropIndex(
                name: "IX_TaskToDo_TaskListId",
                table: "TaskToDo");

            migrationBuilder.DropColumn(
                name: "TaskListId",
                table: "TaskToDo");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "TaskToDo",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 128,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "TaskToDo",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 1024,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserIdentificator",
                table: "TaskToDo",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Priority",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 32,
                oldNullable: true);
        }
    }
}
