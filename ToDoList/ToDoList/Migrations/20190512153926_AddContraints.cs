﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ToDoList.Migrations
{
    public partial class AddContraints : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskToDo_TaskList_TaskListId",
                table: "TaskToDo");

            migrationBuilder.DropIndex(
                name: "IX_TaskList_UserName",
                table: "TaskList");

            migrationBuilder.AlterColumn<int>(
                name: "TaskListId",
                table: "TaskToDo",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "TaskToDo",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 128,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserName",
                table: "TaskList",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Priority",
                maxLength: 32,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TaskList_UserName",
                table: "TaskList",
                column: "UserName",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskToDo_TaskList_TaskListId",
                table: "TaskToDo",
                column: "TaskListId",
                principalTable: "TaskList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskToDo_TaskList_TaskListId",
                table: "TaskToDo");

            migrationBuilder.DropIndex(
                name: "IX_TaskList_UserName",
                table: "TaskList");

            migrationBuilder.AlterColumn<int>(
                name: "TaskListId",
                table: "TaskToDo",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "TaskToDo",
                maxLength: 128,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 128);

            migrationBuilder.AlterColumn<string>(
                name: "UserName",
                table: "TaskList",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Priority",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 32);

            migrationBuilder.CreateIndex(
                name: "IX_TaskList_UserName",
                table: "TaskList",
                column: "UserName",
                unique: true,
                filter: "[UserName] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_TaskToDo_TaskList_TaskListId",
                table: "TaskToDo",
                column: "TaskListId",
                principalTable: "TaskList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
